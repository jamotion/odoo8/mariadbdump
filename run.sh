#!/bin/bash
set -x
mysqldump -h mariadb -p${MARIADB_ENV_MYSQL_ROOT_PASSWORD} $1 > /backup/$2
